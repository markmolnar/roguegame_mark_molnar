﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RogueGame.Jatek.Jatekter;
using RogueGame.Jatek.Szabalyok;
using RogueGame.Jatek.Megjelenites;
using RogueGame.Jatek.Automatizmus;

namespace RogueGame.Jatek.Keret
{
    class Keret
    {
        const int PALYA_MERET_X = 21;
        const int PALYA_MERET_Y = 11;
        const int KINCSEK_SZAMA = 10;
        JatekTer ter;
        bool jatekVege;
        OrajelGenerator generator;
        int megtalaltKincsek;

        public Keret()
        {
            ter = new JatekTer(PALYA_MERET_X, PALYA_MERET_Y);
            PalyaGeneralas();
            jatekVege = false;
            generator = new OrajelGenerator();
        }
        void PalyaGeneralas()
        {
            Random rand = new Random();
            Fal fal0_0=new Fal(0,0,ter);
            Fal falmax_0 = new Fal(PALYA_MERET_X, 0, ter);
            Fal fal0_max = new Fal(0, PALYA_MERET_Y, ter);
            Fal falmax_max = new Fal(PALYA_MERET_X, PALYA_MERET_Y, ter);
            for (int i = 1; i < PALYA_MERET_X; i++)
            {
                Fal falFent = new Fal(i,0,ter);
                Fal falLent = new Fal(i, PALYA_MERET_Y-1, ter);
            }
            for (int i = 1; i < PALYA_MERET_Y; i++)
            {
                Fal falFent = new Fal(0, i, ter);
                Fal falLent = new Fal(PALYA_MERET_X-1, i, ter);
            }

            for (int i = 0; i < KINCSEK_SZAMA; i++)
            {
                int x=1;
                int y=1;
                while (x==1 && y==1)
                {
                    x = rand.Next(1, PALYA_MERET_X-1);
                    y = rand.Next(1, PALYA_MERET_Y-1);
                }
                Kincs kincs = new Kincs(x,y,ter);
                kincs.KincsFelvetel += KincsFelvetelTortent;
            }
        }
        public void Futtatas()
        {
            Jatekos jatekos = new Jatekos("Béla",1,2,ter);
            jatekos.JatekosValtozas += JatekosValtozasTortent;
            GepiJatekos gepiJatekos = new GepiJatekos("Kati",1,5,ter);
            GonoszGepiJatekos gonoszJatekos = new GonoszGepiJatekos("Laci",5,1,ter);
            KonzolosMegjelenito megjelenito = new KonzolosMegjelenito(ter,0,0);
            //KonzolosMegjelenito megjelenito2 = new KonzolosMegjelenito(jatekos,25,0);
            KonzolosEredmenyAblak eredmenyAblak = new KonzolosEredmenyAblak(0,12,5);
            eredmenyAblak.JatekosFeliratkozas(jatekos);
            eredmenyAblak.JatekosFeliratkozas(gepiJatekos);
            eredmenyAblak.JatekosFeliratkozas(gonoszJatekos);
            generator.Felvetel(megjelenito);
            //generator.Felvetel(megjelenito2);
            generator.Felvetel(gepiJatekos);
            generator.Felvetel(gonoszJatekos);
            do
            {
                try
                {
                    ConsoleKeyInfo key = Console.ReadKey(true);
                    //Console.WriteLine("elindult");
                    if (key.Key == ConsoleKey.LeftArrow)
                    {
                        jatekos.Megy(-1, 0);
                        //gepiJatekos.Mozgas();
                        //gonoszJatekos.Mozgas();
                    }
                    if (key.Key == ConsoleKey.RightArrow)
                    {
                        jatekos.Megy(1, 0);
                        //gepiJatekos.Mozgas();
                        //gonoszJatekos.Mozgas();
                    }
                    if (key.Key == ConsoleKey.UpArrow)
                    {
                        jatekos.Megy(0, -1);
                        //gepiJatekos.Mozgas();
                        //gonoszJatekos.Mozgas();
                    }
                    if (key.Key == ConsoleKey.DownArrow)
                    {
                        jatekos.Megy(0, 1);
                        //gepiJatekos.Mozgas();
                        //gonoszJatekos.Mozgas();
                    }
                    if (key.Key == ConsoleKey.Escape) jatekVege = true;
                }
                catch (MozgasHelyHianyMiattNemSikerultKivetel e)
                {
                    System.Console.Beep(500 + e.Elemek.Count * 100, 100);
                }
                megjelenito.Megjelenites();
               // megjelenito2.Megjelenites();
            } while (!jatekVege);
        }

        void KincsFelvetelTortent(Kincs kincs, Jatekos jatekos)
        {
            megtalaltKincsek += 1;
            if (megtalaltKincsek == KINCSEK_SZAMA)
            {
                jatekVege = true;
            }
        }
        void JatekosValtozasTortent(Jatekos jatekos,int pontszam,int eletero)
        {
            if (eletero == 0)
            {
                jatekVege = true;
            }
        }
    }
}
