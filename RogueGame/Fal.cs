﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RogueGame.Jatek.Jatekter;
using RogueGame.Jatek.Megjelenites;

namespace RogueGame.Jatek.Szabalyok
{
    delegate void KincsKezelo(Kincs kincs,Jatekos jatekos);
    delegate void JatekosValtozasKezelo(Jatekos jatekos,int pontszam,int eletero);

    class MozgasNemSikerultKivetel : Exception
    {
        private JatekElem jatekelem;
        int x;
        int y;

        public JatekElem Ma { get { return jatekelem; } }
        public int X { get { return x; } }
        public int Y { get { return y; } }
        public MozgasNemSikerultKivetel(JatekElem jatekelem, int x,int y)
        {
            this.jatekelem = jatekelem;
            this.x = x;
            this.y = y;
        }
    }

    class MozgasHalalMiattNemSikerultKivetel : MozgasNemSikerultKivetel
    {
        public MozgasHalalMiattNemSikerultKivetel(JatekElem jatekelem, int x, int y) : base(jatekelem, x, y)
        {
        }
    }

    class MozgasHelyHianyMiattNemSikerultKivetel : MozgasNemSikerultKivetel
    {
        List<JatekElem> elemek;
        public List<JatekElem> Elemek { get { return elemek; } }
        public MozgasHelyHianyMiattNemSikerultKivetel(JatekElem jatekelem, int x, int y,List<JatekElem> elemek) : base(jatekelem, x, y)
        {
            this.elemek = elemek;
        }
    }

    class Fal : RogzitettJatekElem,IKirajzolhato
    {
        public Fal(int x, int y, JatekTer ter) : base(x, y, ter)
        {
        }

        public char Alak
        {
            get
            {
                return '\u2593';
            }
        }

        public override double Meret
        {
            get
            {
                return 1.0;
            }
        }

        public override void Utkozes(JatekElem elem)
        {
        }
    }

    class Jatekos : MozgoJatekElem, IKirajzolhato,IMegjelenitheto
    {
        string nev;
        int eletero;
        int pontszam;
        public event JatekosValtozasKezelo JatekosValtozas;

        public string Nev { get { return nev;} }
        public override double Meret
        {
            get
            {
                return 0.2;
            }
        }

        public virtual char Alak
        {
            get
            {
                if (Aktiv == true)
                {
                    return '\u263A';
                }
                else
                {
                    return '\u263B';
                }
            }
        }

        public int[] MegjelenitendoMeret
        {
            get
            {
                int[] meretek = new int[2];
                meretek[0] = ter.MeretX;
                meretek[1] = ter.MeretY;
                return meretek;
            }
        }

        public Jatekos(string nev, int x, int y, JatekTer ter) : base(x, y, ter)
        {
            this.nev = nev;
            eletero = 100;
            pontszam = 0;
           
        }
        public void Serul(int serules)
        {
            if (eletero > 0)
            {
                eletero -= serules;
                if (eletero < 0)
                {
                    eletero = 0;
                    Aktiv = false;
                }
                else if (eletero == 0)
                {
                    Aktiv = false;
                }
                JatekosValtozas?.Invoke(this, pontszam, eletero);
            }
        }
        public void PontotSzerez(int pont)
        {
            pontszam += pont;
            JatekosValtozas?.Invoke(this, pontszam, eletero);
        }

        public void Megy(int rx,int ry)
        {
            int ujx = X + rx;
            int ujy = Y + ry;
            //Console.WriteLine(ujx + "," + ujy);
            Athelyez(ujx,ujy);
        }
        public override void Utkozes(JatekElem elem)
        {
        }

        public List<IKirajzolhato> MegjelenítendőElemek()
        {
            List<JatekElem> elemek=ter.MegadottHelyenLevok(this.X, this.Y, 5);
            List<IKirajzolhato> kirajzolhatok=new List<IKirajzolhato>();
            foreach (JatekElem elem in elemek)
            {
                if (elem is IKirajzolhato)
                {
                    kirajzolhatok.Add(elem as IKirajzolhato);
                }
            }
            return kirajzolhatok;
        }
    }

    class Kincs : RogzitettJatekElem,IKirajzolhato
    {
       public event KincsKezelo KincsFelvetel; 

        public Kincs(int x, int y, JatekTer ter) : base(x, y, ter)
        {
        }

        public char Alak
        {
            get
            {
                return '\u2666';
            }
        }

        public override double Meret
        {
            get
            {
                return 1.0;
            }
        }

        public override void Utkozes(JatekElem elem)
        {
            if (elem is Jatekos)
            {
                (elem as Jatekos).PontotSzerez(50);
                ter.Torles(this);
                KincsFelvetel?.Invoke(this, elem as Jatekos);
            }
            
        }

    }
}
