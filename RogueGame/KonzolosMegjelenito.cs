﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RogueGame.Jatek.Szabalyok;
using RogueGame.Jatek.Automatizmus;

namespace RogueGame.Jatek.Megjelenites
{
    public interface IKirajzolhato
    {
        int X { get; }
        int Y { get; }
        char Alak{ get; }
    }
    public interface IMegjelenitheto
    {
        int[] MegjelenitendoMeret { get; }
        List <IKirajzolhato> MegjelenítendőElemek();
    }

    public class KonzolosMegjelenito:IAutomatikusanMukodo
    {
        IMegjelenitheto forras;
        int pozX;
        int pozY;

        public KonzolosMegjelenito(IMegjelenitheto forras,int pozX, int pozY)
        {
            this.forras = forras;
            this.pozX = pozX;
            this.pozY = pozY;
        }

        public int MukodesIntervallum
        {
            get
            {
                return 1;
            }
        }

        public void Megjelenites()
        {
            List<IKirajzolhato> kirajzolhato=forras.MegjelenítendőElemek();
            int[] teruletMerete = forras.MegjelenitendoMeret;
            for (int i = 0; i < teruletMerete[0]; i++)
            {
                for (int j = 0; j < teruletMerete[1]; j++)
                {
                    int counter = 0;
                    for (int g=0;g<kirajzolhato.Count;g++)
                    {
                        if(kirajzolhato[g].X+pozX==i+pozX && kirajzolhato[g].Y+pozY == j+pozY)
                        {
                            SzalbiztosKonzol.KiirasXY(i+pozX, j+pozY, kirajzolhato[g].Alak);
                            break;
                        }
                        else if(counter==(kirajzolhato.Count-1))
                        {
                            SzalbiztosKonzol.KiirasXY(i+pozX, j+pozY, ' ');
                        }
                        counter++;
                    }
                }
            }

        }

        public void Mukodik()
        {
            Megjelenites();
        }
    }
    class KonzolosEredmenyAblak
    {
        int pozX;
        int pozY;
        int maxSorSzam;
        int sor;
        public KonzolosEredmenyAblak()
        {
            pozX = 40;
            pozY = 41;
            maxSorSzam = 5;
            sor = 0;
        }
        public KonzolosEredmenyAblak(int pozx, int pozy,int maxsor)
        {
            pozX = pozx;
            pozY = pozy;
            maxSorSzam = maxsor;
            sor = 0;
        }
        void JatekosValtozasTortent(Jatekos jatekos,int pontszam, int eletero)
        {
            StringBuilder builder = new StringBuilder();
            string eredmeny = builder.Append("Játékos neve:").Append(jatekos.Nev).Append(" ").Append("pontszáma:")
                .Append(pontszam).Append(" ").Append("életereje:").Append(eletero).ToString();
            SzalbiztosKonzol.KiirasXY(pozX, pozY + sor, System.Environment.NewLine);
            SzalbiztosKonzol.KiirasXY(pozX,pozY+sor,eredmeny);
            sor += 1;
            if (sor == maxSorSzam)
            {
                sor = 0;
            }
        }
        public void JatekosFeliratkozas(Jatekos jatekos)
        {
            jatekos.JatekosValtozas += JatekosValtozasTortent;
        }
    }
}
